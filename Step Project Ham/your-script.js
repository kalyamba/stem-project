var grid = document.querySelector('.gallery-grid');
var msnry = new Masonry(grid, {
  itemSelector: '.gallery-grid img',
  columnWidth: '.gallery-grid img',
  gutter: 10,
  percentPosition: true
});
