const tabs = document.querySelectorAll('.tab');
tabs.forEach(tab => {
  const radio = tab.querySelector('input[type="radio"]');
  const label = tab.querySelector('label');
  const content = tab.querySelector('.tab-content');
  tab.addEventListener('click', () => {

    tabs.forEach(tab => {
      tab.classList.remove('active');
      tab.querySelector('.tab-content').style.display = 'none';
    });

    tab.classList.add('active');
    content.style.display = 'block';
    radio.checked = true;
  });
});


const buttons = document.querySelectorAll('.buttons button');
const gallery = document.querySelector('.gallery');
const loadMoreBtn = document.querySelector('.load-more');
const maxImages = 12;
const maxTotalImages = 36;
let loadedImages = maxImages;
let totalImages = maxImages;
let loadMoreClicks = 0;

const preloader = document.createElement('div');
preloader.classList.add('preloader');

buttons.forEach(button => {
  button.addEventListener('click', () => {
    buttons.forEach(btn => btn.classList.remove('active'));
    button.classList.add('active');
    const filter = button.getAttribute('data-filter');
    gallery.querySelectorAll('.photo-item').forEach(item => {
      item.style.display = 'none';
      if (filter === 'all' || item.getAttribute('data-tags').split(' ').includes(filter)) {
        item.style.display = 'block';
      }
    });
    loadedImages = maxImages;
    totalImages = maxImages;
    loadMoreClicks = 0;
    loadMoreBtn.classList.remove('hidden');
  });
});

loadMoreBtn.addEventListener('click', () => {
  if (totalImages >= maxTotalImages || loadMoreClicks >= 2) {
    loadMoreBtn.classList.add('hidden');
    return;
  }

  loadMoreBtn.replaceWith(preloader);
  document.body.classList.add('loading');
  setTimeout(() => {
    document.body.classList.remove('loading');
  }, 2000);
  setTimeout(() => {
    for (let i = 1; i <= maxImages; i++) {
      if (totalImages + i > maxTotalImages) {
        break;
      }
      const img = document.createElement('img');
      img.src = `./load-more/image${totalImages + i}.png`;
      img.setAttribute('data-tags', 'graphic-design');
      img.classList.add('photo-item');
      img.style.width = '285px';
      img.style.height = '206px';
      gallery.appendChild(img);
    }

    totalImages += maxImages;
    loadMoreClicks++;

    if (totalImages >= maxTotalImages || loadMoreClicks >= 2) {
      loadMoreBtn.classList.add('hidden');
    }

    preloader.replaceWith(loadMoreBtn);
  }, 2000);
});











const carousel = document.querySelector('.carousel');
const carouselItems = document.querySelectorAll('.carousel-item');
const mainImage = document.querySelector('.main-image img');
const arrows = document.querySelectorAll('.arrow');
const personInfos = document.querySelectorAll('.person-info');

const peopleData = [
  {
    name: "John Doe",
    profession: "Software Engineer",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  },
  {
    name: "Jane Doe",
    profession: "Designer",
    description: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium."
  },
  {
    name: "Bob Smith",
    profession: "Project Manager",
    description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident."
  },
  {
    name: "Mary Johnson",
    profession: "Marketing Specialist",
    description: "Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit."
  }
];

let currentSlide = 0;
const totalSlides = carouselItems.length;

function switchSlide(index) {
  carouselItems.forEach(item => {
    item.classList.remove('active');
  });
  carouselItems[index].classList.add('active');
  mainImage.src = carouselItems[index].querySelector('img').src;
  const currentPerson = peopleData[index];
  const currentPersonInfo = personInfos[0];
  currentPersonInfo.querySelector('p.name-info').textContent = currentPerson.name;
  currentPersonInfo.querySelector('p.description').textContent = currentPerson.description;
  currentPersonInfo.querySelector('p.profession').textContent = currentPerson.profession;
}


arrows.forEach(arrow => {
  arrow.addEventListener('click', event => {
    if (event.target.classList.contains('arrow-left')) {
      currentSlide = (currentSlide - 1 + totalSlides) % totalSlides;
    } else if (event.target.classList.contains('arrow-right')) {
      currentSlide = (currentSlide + 1) % totalSlides;
    }
    switchSlide(currentSlide);
  });
});


carouselItems.forEach((item, index) => {
  item.addEventListener('click', () => {
    currentSlide = index;
    switchSlide(currentSlide);
  });
});


switchSlide(currentSlide);



window.onload = () => {
  jQuery(document).ready(function ($) {
    const $grid = $('.elements-gride').masonry({
      itemSelector: '.element-item',
      columnWidth: 290,
    });

    let counter = 0;

    $('.load-more-2').click(function () {
      $('#loader-2').show();


      setTimeout(function () {
        for (let i = 1; i <= 20; i++) {
          const imgSrc = `./Gallery/house${i}.png`;
          const $img = $(`<img class="element-item" src="${imgSrc}">`);
          $grid.append($img).masonry('appended', $img);
        }

        counter++;
        if (counter === 2) {
          $('.load-more-2').hide();
        }

        $('#loader-2').hide();
      }, 2000);
    });
  });
};






